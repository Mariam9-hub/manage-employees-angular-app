import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EmployeeFormComponent } from './employees/employee-form/employee-form.component';
import { EmployeeComponent } from './employees/employee/employee.component';
import { EmployeesComponent } from './employees/employees.component';

const routes: Routes = [
  { path: 'employees', component: EmployeesComponent },
  { path: 'employees/new', component: EmployeeFormComponent },
  { path: '**', redirectTo: 'employees' },
  { path: '', redirectTo: 'employees', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
