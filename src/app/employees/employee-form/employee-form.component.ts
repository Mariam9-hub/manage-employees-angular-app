import { HttpErrorResponse } from '@angular/common/http';
import { Component, Inject, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { Employee } from 'src/app/models/employee.model';
import { EmployeeService } from 'src/app/services/employee.service';

@Component({
  selector: 'app-employee-form',
  templateUrl: './employee-form.component.html',
  styleUrls: ['./employee-form.component.scss'],
})
export class EmployeeFormComponent implements OnInit {
  empForm: FormGroup;
  employee: Employee;
  constructor(
    private formBuilder: FormBuilder,
    private empService: EmployeeService,
    private router: Router,
    @Inject(MAT_DIALOG_DATA) public data: Partial<Employee>,
    public dialogRef: MatDialogRef<EmployeeFormComponent>
  ) {}

  ngOnInit() {
    console.log('Employee form');
    if (this.data) {
      this.empService.getEmployeeById(this.data?.id).subscribe(
        (data: Employee) => {
          console.log('employeerr to edit', data);
          this.employee = data;
          this.empForm.get('name').setValue(data?.name);
          this.empForm.get('email').setValue(data?.email);
          this.empForm.get('jobTitle').setValue(data?.jobTitle);
          this.empForm.get('phone').setValue(data?.phone);
          this.empForm.get('imageUrl').setValue(data?.imageUrl);
        },
        (error: HttpErrorResponse) => {
          console.log(error.message);
        }
      );
    }

    this.initForm();
  }
  initForm() {
    this.empForm = this.formBuilder.group({
      name: ['', [Validators.required]],
      email: ['', [Validators.email, Validators.required]],
      jobTitle: ['', [Validators.required]],
      phone: ['', [Validators.required]],
      imageUrl: [''],
    });
  }
  onSaveEmployee() {
    console.log('form value', this.empForm.value);
    if (this.data && this.data.id) {
      this.empService
        .updateEmployee({
          ...this.employee,
          name: this.empForm.get('name').value,
          email: this.empForm.get('email').value,
          jobTitle: this.empForm.get('jobTitle').value,
          phone: this.empForm.get('phone').value,
          imageUrl: this.empForm.get('imageUrl').value,
        })
        .subscribe(
          () => {
            this.dialogRef.close('update employee with success');
            this.router.navigate(['employees']);
          },

          (error: HttpErrorResponse) => {
            console.log('error' + error.message);
          }
        );
    } else {
      this.empService.addEmployee(this.empForm.value).subscribe(
        () => {
          this.dialogRef.close('save with success');
          this.router.navigate(['employees']);
        },
        (error: HttpErrorResponse) => {
          console.log('error' + error.message);
        }
      );
    }
  }
}
