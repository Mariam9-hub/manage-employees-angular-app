import { Component, OnInit, OnDestroy } from '@angular/core';
import { EmployeeService } from '../services/employee.service';
import { Employee } from '../models/employee.model';
import { HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { EmployeeFormComponent } from './employee-form/employee-form.component';
import { HeaderComponent } from '../header/header.component';
import { Subscription } from 'rxjs';
@Component({
  selector: 'app-employees',
  templateUrl: './employees.component.html',
  styleUrls: ['./employees.component.scss'],
})
export class EmployeesComponent implements OnInit, OnDestroy {
  constructor(
    private empService: EmployeeService,
    private router: Router,
    public dialog: MatDialog
  ) {}
  loading: boolean = true;
  employees: Employee[];
  employeesSubscription: Subscription;
  ngOnInit() {
    this.getEmployees();
    console.log('employees', this.employees);
  }
  ngAfterViewInit() {}

  getEmployees() {
    this.employeesSubscription = this.empService.employeesSubject.subscribe(
      (data) => {
        this.employees = data;
        this.loading = false;
      }
    );
    this.empService.getEmployees();
  }
  onAddEmp() {
    console.log('helooooo');

    const dialogRef = this.dialog.open(EmployeeFormComponent, {
      disableClose: true,
    });

    dialogRef.afterClosed().subscribe((result) => {
      this.getEmployees();
      console.log('Dialog closed');
    });
  }
  addItem(event: any) {
    console.log('getEmployees', event);

    this.getEmployees();
  }
  ngOnDestroy() {
    this.employeesSubscription.unsubscribe();
  }
}
