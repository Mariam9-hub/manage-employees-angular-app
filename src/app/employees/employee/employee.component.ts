import { HttpErrorResponse } from '@angular/common/http';
import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { EmployeeService } from 'src/app/services/employee.service';
import { EmployeeFormComponent } from '../employee-form/employee-form.component';

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.scss'],
})
export class EmployeeComponent implements OnInit {
  @Input() name;
  @Input() email;
  @Input() phone;
  @Input() jobTitle;
  @Input() imageUrl;
  @Input() id;
  @Output() newEmployeeEvent = new EventEmitter<boolean>();
  constructor(
    private empService: EmployeeService,
    public dialog: MatDialog,
    private router: Router
  ) {}

  ngOnInit() {
    this.imageUrl === ''
      ? (this.imageUrl =
          'https://png.pngtree.com/png-clipart/20190520/original/pngtree-who-icon-png-image_3568639.jpg')
      : null;
  }
  onDelete() {
    console.log('id', this.id);

    if (confirm('Are you sure you want to delete this employee ?')) {
      this.empService.deleteEmployee(this.id).subscribe(
        () => {
          console.log('delete successfully');
          this.newEmployeeEvent.emit(true);
        },
        (error: HttpErrorResponse) => {
          console.log('error :' + error.message);
        }
      );
    }
  }
  onEdit() {
    console.log('editId', this.id);

    const dialogRef = this.dialog.open(EmployeeFormComponent, {
      disableClose: true,
      data: {
        id: this.id,
      },
    });

    dialogRef.afterClosed().subscribe((result) => {
      this.newEmployeeEvent.emit(true);
      console.log('Dialog closed');
    });
  }
}
