import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { Employee } from '../models/employee.model';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class EmployeeService {
  constructor(private http: HttpClient) {}

  private apiServerUrl = environment.apiBaseUrl;
  employees: Employee[];
  employeesSubject = new Subject<Employee[]>();

  emitEmployees() {
    this.employeesSubject.next(this.employees);
  }
  getEmployees() {
    return this.http
      .get<Employee[]>(`${this.apiServerUrl}/employee/all`)
      .subscribe(
        (data) => {
          this.employees = data ? data : [];
          this.emitEmployees();
        },
        (error: HttpErrorResponse) => {
          console.log('error :' + error.message);
        }
      );
  }

  getEmployeeById(id: number): Observable<Employee> {
    return this.http.get<Employee>(`${this.apiServerUrl}/employee/find/${id}`);
  }

  addEmployee(employee: Partial<Employee>) {
    return this.http.post<Employee>(
      `${this.apiServerUrl}/employee/add`,
      employee
    );
  }

  updateEmployee(employee: Employee): Observable<Employee> {
    return this.http.put<Employee>(
      `${this.apiServerUrl}/employee/update`,
      employee
    );
  }

  deleteEmployee(id: number): Observable<void> {
    return this.http.delete<void>(`${this.apiServerUrl}/employee/delete/${id}`);
  }
}
