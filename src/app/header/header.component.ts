import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Subscription } from 'rxjs';
import { Employee } from '../models/employee.model';
import { EmployeeService } from '../services/employee.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit, OnDestroy {
  search = new FormControl('');
  employees: Employee[];
  employeesSubcription: Subscription;
  constructor(private empService: EmployeeService) {}

  ngOnInit() {
    this.empService.employeesSubject.subscribe((data) => {
      this.employees = data;
    });
    this.search.valueChanges.subscribe((value: string) => {
      let results: Employee[] = [];
      console.log('value', value);
      for (let index = 0; index < this.employees.length; index++) {
        const employee = this.employees[index];
        console.log(
          'condition',
          employee['name'].toLowerCase().includes(value.toLowerCase())
        );

        if (
          employee['name'].toLowerCase().includes(value.toLowerCase()) ||
          employee['email'].toLowerCase().includes(value.toLowerCase()) ||
          employee['phone'].toLowerCase().includes(value.toLowerCase()) ||
          employee['jobTitle'].toLowerCase().includes(value.toLowerCase())
        ) {
          results.push(employee);
          console.log(results);
        }
      }
      if (results.length !== 0 && value !== '') {
        this.empService.employees = results;
        this.empService.emitEmployees();
      } else {
        this.empService.getEmployees();
      }
    });
  }
  ngOnDestroy() {
    this.employeesSubcription.unsubscribe();
  }
}
